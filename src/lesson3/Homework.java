package lesson3;

public class Homework {

    public static void main(String[] args) {

        System.out.println("Task 1\n" + "Create an array and fill it with 2 number.");
        int[] numberArray = new int[10];

        for (int i = 0; i < 10; i++) {
            numberArray[i] = 2;
            System.out.print(numberArray[i] + " ");
        }
        System.out.println();

        System.out.println("\nTask 2\n" + "Create an array and fill it with numbers from 1:1000.");
        int[] numbers = new int[1000];
        for (int i = 0; i < 1000; i++) {
            numbers[i] = i + 1;
            System.out.print(numbers[i] + " ");
        }
        System.out.println();

        System.out.println("\nTask 3\n" + "Create an array and fill it with odd numbers from -20:20");
        int[] oddNumbers = new int[20];
        for (int i = -20, index = 0; i <= 20; i++) {
            if (i % 2 != 0) {
                oddNumbers[index++] = i;
            }
        }
        for (int i = 0; i < 20; i++) {
            System.out.print(oddNumbers[i] + " ");
        }
        System.out.println();

        System.out.println("\nTask 4\n" + "Create an array and fill it. Print all elements which can be divided by 5.");
        int[] dividedByFive = {6, 5, 25, 7, 56, 90, -30, 43};
        for (int i = 0; i < dividedByFive.length; i++) {
            if (dividedByFive[i] % 5 == 0) {
                System.out.print(dividedByFive[i] + " ");
            }
        }
        System.out.println();

        System.out.println("\nTask 5\n" + "Create an array and fill it. Print all elements which are between 24.12 and 467.23.");
        double[] arrayBetweenNumbers = {56.3, 34.6, 123.89, 12.8, 3, 398.5};
        for (int i = 0; i < arrayBetweenNumbers.length; i++) {
            if (arrayBetweenNumbers[i] >= 24.12 && arrayBetweenNumbers[i] <= 467.23) {
                System.out.print(arrayBetweenNumbers[i] + "  ");
            }
        }
        System.out.println();

        System.out.println("\nTask 6\n" + "Create an array and fill it. Print count of elements which can be divided by 2.");
        int[] array = {1, -3, 4, -8, 0, 4, 5, 9};
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0)
                count++;
        }
        System.out.println(count);
        System.out.println();

        System.out.println("\nTask 8\n" + "Get number from array which doesn't have its pair.");
        int[] arrayWithPairs = {2, 4, 5, 6, 5, 2, 4};
        int el = arrayWithPairs[0];
        for (int i = 1; i < arrayWithPairs.length; i++) {
            el = el ^ arrayWithPairs[i];
        }
        System.out.println(el);
    }
}
