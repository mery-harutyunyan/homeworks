package lesson7;

import lesson7.Model.Product;
import lesson7.Service.ProductService;

import java.util.Scanner;

public class Homework {
    public static void main(String[] args) {
        ProductService service = new ProductService();
        Product product1 = service.createProduct();
        Product product2 = service.createProduct();
        Product product3 = service.createProduct();
        Product[] products = {product1, product2, product3};


        boolean isMenuActive = true;
        while (isMenuActive) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter command number");
            System.out.println("1. Print yes if discount is greater 20%");
            System.out.println("2. Print name of expansive product");
            System.out.println("3. Print product with longest name");
            System.out.println("4. Print Names Of Active Products With Discount Greater than 25%");
            System.out.println("5. Return cheapest product");
            System.out.println("6. Print Count Of Not Active products");
            System.out.println("7. Print products with Discount Greater than 20%");
            System.out.println("8. Return cheapest product from actives");
            System.out.println("9. Return products ordered By Price");
            System.out.println("10. Exit");

            int commandNumber = scanner.nextInt();

            switch (commandNumber) {
                case 1:
                    service.printYesDiscountGreater20(product1);
                    break;
                case 2:
                    System.out.println(service.nameOfExpansive(product1, product2));
                    break;
                case 3:
                    service.printLongestName(product1, product2, product3);
                    break;
                case 4:
                    service.printNamesOfActiveWithDiscountGreater25(products);
                    break;
                case 5:
                     service.cheapestProduct(products).printInfo();
                    break;
                case 6:
                    service.printCountOfNotActives(products);
                    break;
                case 7:
                    service.printArrayInfo(service.productsDiscountGreater20(products));
                    break;
                case 8:
                    service.cheapestFromActives(products).printInfo();
                    break;
                case 9:
                    service.printArrayInfo(service.orderByPrice(products));
                    break;
                case 10:
                    isMenuActive = false;
                    System.out.println("Good bye");
                    break;
                default:
                    System.out.println("Invalid command number");
            }

        }

    }
}
