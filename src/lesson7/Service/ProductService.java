package lesson7.Service;

import lesson7.Model.Product;

import java.util.Scanner;

public class ProductService {

    public Product createProduct() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter product name");
        String name = scanner.next();

        System.out.println("Enter product category");
        String category = scanner.next();

        System.out.println("Enter product brand");
        String brand = scanner.next();

        System.out.println("Enter product price");
        float price = scanner.nextFloat();

        System.out.println("Enter product quantity");
        int quantity = scanner.nextInt();

        System.out.println("Enter product is active(y/n)");
        boolean isActive = scanner.next().charAt(0) == 'y';

        Product product = new Product(name, category, price);
        product.setQuantity(quantity);
        product.setBrand(brand);
        product.setActive(isActive);

        return product;
    }

    public void printYesDiscountGreater20(Product product) {
        System.out.println(product.getDiscount() > 20 ? "Yes" : "No");
    }

    public String nameOfExpansive(Product product1, Product product2) {
        return product1.getPrice() > product2.getPrice() ? product1.getName() : product2.getName();
    }

    public void printLongestName(Product product1, Product product2, Product product3) {
        Product withLongestName = product1.getName().length() > product2.getName().length() ? product1 : product2;

        if (withLongestName.getName().length() < product3.getName().length())
            withLongestName = product3;

        System.out.println(withLongestName.getName());
    }

    public void printNamesOfActiveWithDiscountGreater25(Product[] products) {
        for (Product product : products) {
            if (product.isActive() && product.getDiscount() > 25) {
                System.out.println(product.getName());
            }
        }
    }


    public Product cheapestProduct(Product[] products) {

        Product cheapest = products[0];

        for (int i = 1; i < products.length; i++) {
            if (products[i].getPrice() < cheapest.getPrice()) {
                cheapest = products[i];
            }
        }

        return cheapest;
    }

    public void printCountOfNotActives(Product[] products) {
        int count = 0;

        for (Product product : products) {
            if (!product.isActive())
                count++;
        }
        System.out.println("Count of not actives is " + count);
    }

    public Product[] productsDiscountGreater20(Product[] products) {
        int count = 0;
        for (Product product : products) {
            if (product.getDiscount() > 20)
                count++;
        }

        Product[] result = new Product[count];
        int index = 0;

        for (Product product : products) {
            if (product.getDiscount() > 20)
                result[index++] = product;
        }

        return result;
    }

    public Product cheapestFromActives(Product[] products) {
        //find first active
        Product product = null;

        for (int i = 0; i < products.length; i++) {
            if (products[i].isActive()) {
                if (product == null) {
                    product = products[i];
                } else if (product.getPrice() > products[i].getPrice()) {
                    product = products[i];
                }
            }
        }

        return product;
    }

    public Product[] orderByPrice(Product[] products) {
        boolean isActive = true;
        int forCount = 0;

        while (isActive) {
            isActive = false;
            for (int i = 0; i < products.length - 1 - forCount; i++) {
                if (products[i].getPrice() > products[i + 1].getPrice()) {
                    Product temp = products[i];
                    products[i] = products[i + 1];
                    products[i + 1] = temp;
                    isActive = true;
                }
            }

            forCount++;
        }

        return products;
    }

    public void printArrayInfo(Product[] products) {
        System.out.println("------------------------");
        for (int i = 0; i < products.length; i++) {
            System.out.printf("%d. Product name : %s %n", (i + 1), products[i].getName());
        }
        System.out.println("------------------------");
    }
}
