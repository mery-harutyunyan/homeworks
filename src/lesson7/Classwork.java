package lesson7;

public class Classwork {
    public static void main(String[] args) {
        String s = "debed";

        Classwork test = new Classwork();
        System.out.println(test.isPalindrome(s));
    }

    public boolean isPalindromeMyVersion(String s) {

        if (s.length() == 0 || s.length() == 1)
            return true;
        if (s.charAt(0) == s.charAt(s.length() - 1))
            return isPalindrome(s.substring(1, s.length() - 1));

        return false;
    }

    public boolean isPalindrome(String s) {
        int start = 0;
        int end = s.length() - 1;
        while (start < end) {
            if (s.charAt(start) != s.charAt(end)) {
                return false;

            }
            start++;
            end--;

        }
        return true;
    }
}
