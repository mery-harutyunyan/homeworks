package lesson12;

import java.util.ArrayList;
import java.util.Arrays;

public class main {
    public static void main(String[] args) {
        ArrayList<Integer> myList = new ArrayList<>(Arrays.asList(4, 6, 8, 23, 2, 21, 5, 75, 34));

        for (int i = 0; i < myList.size(); i++) {
            if (myList.get(i) % 2 == 0) {
                myList.remove(i);
                i--;
            }
        }

        for (int i = myList.size()-1; i >=0 ; i++) {
            if (myList.get(i) % 2 == 0) {
                myList.remove(i);
            }
        }


        System.out.println(myList);
    }


}
