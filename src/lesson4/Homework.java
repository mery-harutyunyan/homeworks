package lesson4;

public class Homework {
    public static void main(String[] args) {

        Product chair = new Product("chair", 12000);
        chair.setBrand("IKEA");
        chair.setDiscount(10);
        System.out.printf("%s %s have %.2f%% discount today", chair.getBrand(), chair.getName(), chair.getDiscount());
        System.out.println();

        Product dress = new Product("Little black dress", 23000);
        dress.setBrand("ZARA");
        dress.setQuantity(15);
        System.out.printf("%s have %d new %s", dress.getBrand(), dress.getQuantity(), dress.getName());
        System.out.println();


        System.out.printf("%nBubble sort%n");
        int[] array = {2, 6, 3, 8, 0, -1, 3};

        boolean isNotComplete = true;
        int forCounts = 0;

        while (isNotComplete) {
            isNotComplete = false;
            for (int i = 0; i < array.length - 1 - forCounts; i++) {
                if (array[i] > array[i + 1]) {
                    isNotComplete = true;
                    int tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                }
            }
            forCounts++;
        }


        for (int x : array) {
            System.out.print(x + " ");
        }

    }
}
