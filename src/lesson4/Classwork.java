package lesson4;

public class Classwork {
    public static void main(String[] args) {
        int[] a = {7, 4, 5, -2, 6, 4};

        for (int i = 1; i < a.length; i++) {
            if (a[0] > a[i]) {
                int x = a[i];
                a[0] = a[i];
                a[i] = x;
            }
        }

        System.out.println(a[0]);
    }
}
