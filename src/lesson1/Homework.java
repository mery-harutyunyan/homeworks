package lesson1;

public class Homework {

    public static void main(String[] args) {
        boolean javaBoolean = true;
        byte javaByte = 123;
        short javaShort = 12312;
        int javaInt = 123123123;
        long javaLong = 123123123123123L;
        double javaDouble = 12.3;
        float javaFloat = 12.3f;
        char javaChar = 'M';
        String javaStringDataType = "Data Type is ";
        String javaStringDefaultValue = ", default value is ";
        String javaStringDefaultSize = ", default size is ";
        String javaStringExample = ", example ";

        System.out.println(javaStringDataType + "boolean" + javaStringDefaultValue + "false" + javaStringDefaultSize + "1 bit" + javaStringExample + javaBoolean);
        System.out.println(javaStringDataType + "byte" + javaStringDefaultValue + "0" + javaStringDefaultSize + "1 byte" + javaStringExample + javaByte);
        System.out.println(javaStringDataType + "short" + javaStringDefaultValue + "0" + javaStringDefaultSize + "2 byte" + javaStringExample + javaShort);
        System.out.println(javaStringDataType + "int" + javaStringDefaultValue + "0" + javaStringDefaultSize + "4 byte" + javaStringExample + javaInt);
        System.out.println(javaStringDataType + "long" + javaStringDefaultValue + "0L" + javaStringDefaultSize + "8 byte" + javaStringExample + javaLong);
        System.out.println(javaStringDataType + "double" + javaStringDefaultValue + "0.0d" + javaStringDefaultSize + "8 byte" + javaStringExample + javaDouble);
        System.out.println(javaStringDataType + "float" + javaStringDefaultValue + "0.0f" + javaStringDefaultSize + "4 byte" + javaStringExample + javaFloat);
        System.out.println(javaStringDataType + "char" + javaStringDefaultValue + "\\u0000" + javaStringDefaultSize + "2 byte" + javaStringExample + javaChar);
    }
}
