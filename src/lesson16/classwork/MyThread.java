package lesson15.classwork.classwork;

public class MyThread extends Thread {
    private String name;
    private Counter counter;

    public MyThread(String name, Counter counter) {
        this.name = name;
        this.counter = counter;
    }

    public void run(){
        System.out.println("Run for thread " + name + " started");
        counter.count(name);
        System.out.println("Run for thread " + name + " ended");
    }
}
