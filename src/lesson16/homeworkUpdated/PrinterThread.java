package lesson16.homeworkUpdated;

public class PrinterThread extends Thread {
    private CounterThread counterThread;

    public PrinterThread(CounterThread counterThread) {
        this.counterThread = counterThread;
    }

    public void run() {
        do {
            System.out.println("Folder size is : " + counterThread.folderSize);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (counterThread.isAlive());

        System.out.println("Folder size is : " + counterThread.folderSize);
    }
}
