package lesson16.homeworkUpdated;

import java.io.File;

public class CounterThread extends Thread {
    public long folderSize;
    public File folder;

    public CounterThread(File folder) {
        this.folder = folder;
    }

    public void run() {
        getSize(folder);
    }

    public void getSize(File folder) {
        File[] files = folder.listFiles();

        assert files != null;

        for (File file : files) {
            if (file.isFile()) {
                folderSize += file.length();
            }
            if (file.isDirectory()) {
                getSize(file);
            }
        }
    }
}
