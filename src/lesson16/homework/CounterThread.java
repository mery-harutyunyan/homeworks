package lesson16.homework;

import java.io.File;

public class CounterThread extends Thread {
    private File folder;
    public static long size;

    public CounterThread(File folder) {
        this.folder = folder;
    }

    public void run() {
        File[] files = folder.listFiles();

        assert files != null;

        for (File file : files) {
            if (file.isFile()) {
                size += file.length();
            }
            if (file.isDirectory()) {
                CounterThread counterThread = new CounterThread(file);
                counterThread.start();
                try {
                    counterThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Size : " + size);
    }
}
