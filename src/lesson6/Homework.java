package lesson6;

public class Homework {
    public static void main(String[] args) {

        //get Fibonacci sequence number by order without recursion
        Homework homework = new Homework();
        System.out.println(homework.Fibonacci(3));
    }

    public int Fibonacci(int n) {
        int a = 1;
        int b = 1;
        int c = 1;
        for (int i = 2; i < n; i++) {
            a = b;
            b = c;
            c = a + b;
        }
        return c;
    }
}
