package lesson6;

public class Classwork {
    public static void main(String[] args) {
        String password = "my12hjg343jhh";

        int count = 0;
        int len = password.length();
        for (int i = 0; i < len; i++) {
            if (password.charAt(i) >= 48 && password.charAt(i) <= 57) {
                count++;
            }
        }

        System.out.println(count);

    }
}
