package lesson5;

import lesson4.Product;

public class ElectricProduct extends Product {
    private float voltage;
    private double power;

    public ElectricProduct(String name, float price) {
        super(name, price);
    }

    public float getVoltage() {
        return voltage;
    }

    public void setVoltage(float voltage) {
        this.voltage = voltage;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Product Voltage: %.2f %n", voltage);
        System.out.printf("Product Power: %.2f %n", power);
    }
}
