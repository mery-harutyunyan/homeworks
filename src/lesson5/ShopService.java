package lesson5;

public class ShopService {

    public void printProductHavingMaxPrice(Shop shop) {
        ElectricProduct[] electronics = shop.getElectricProducts();

        ElectricProduct max = electronics[0];
        for (int i = 1; i < electronics.length; i++) {
            if (max.getPrice() < electronics[i].getPrice()) {
                max = electronics[i];
            }
        }

        System.out.println("---------------------------");
        System.out.println("Product having max price is");
        System.out.println(max.getName());
        System.out.println("Price is ");
        System.out.println((int) max.getPrice());
    }
}
