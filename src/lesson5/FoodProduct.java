package lesson5;

import lesson4.Product;

public class FoodProduct extends Product {
    private String date;
    private String expiredDate;

    public FoodProduct(String name, float price, String expiredDate) {
        super(name, price);
        this.expiredDate = expiredDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Product Date: %s %n", date);
        System.out.printf("Product Expired Date: %s %n", expiredDate);
    }
}
