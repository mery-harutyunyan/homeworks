package lesson5;

import lesson4.Product;

public class ClothProduct extends Product {
    private int size;
    private String color;

    public ClothProduct(String name, float price) {
        super(name, price);
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Product Size: %d %n", size);
        System.out.printf("Product Color: %s %n", color);
    }
}
