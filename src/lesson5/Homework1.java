package lesson5;

import java.util.Scanner;

public class Homework1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Shop shop = new Shop();
        shop.setName("Zigzag");

        System.out.println("Enter new products count");
        int count = scanner.nextInt();

        ElectricProduct[] electronics = new ElectricProduct[count];
        for (int i = 0; i < electronics.length; i++) {
            System.out.println("Enter name for electronics " + (i + 1));
            String name = scanner.next();

            System.out.println("Enter price for electronics " + (i + 1));
            float price = scanner.nextFloat();
            electronics[i] = new ElectricProduct(name, price);
        }
        shop.setElectricProducts(electronics);

        ShopService service = new ShopService();
        service.printProductHavingMaxPrice(shop);
    }
}
