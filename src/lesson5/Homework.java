package lesson5;

import java.util.Scanner;

public class Homework {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        FoodProduct food = new FoodProduct("Cheese", 1500, "2021-04-12");
        System.out.printf("Set %s discount%n", food.getName());
        food.setDiscount(scanner.nextFloat());
        System.out.printf("Set %s date%n", food.getName());
        food.setDate(scanner.next());

        ElectricProduct electronics = new ElectricProduct("Washing Machine", 220000);
        System.out.printf("Set %s discount%n", electronics.getName());
        electronics.setDiscount(scanner.nextFloat());
        System.out.printf("Set %s voltage%n", electronics.getName());
        electronics.setVoltage(scanner.nextFloat());
        System.out.printf("Set %s power%n", electronics.getName());
        electronics.setPower(scanner.nextDouble());

        food.printInfo();
        System.out.println();
        electronics.printInfo();

    }
}
