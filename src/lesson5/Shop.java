package lesson5;

public class Shop {
    private String name;
    private ElectricProduct[] electricProducts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ElectricProduct[] getElectricProducts() {
        return electricProducts;
    }

    public void setElectricProducts(ElectricProduct[] electricProducts) {
        this.electricProducts = electricProducts;
    }
}
