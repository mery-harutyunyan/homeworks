package lesson10.Model;

public class Student {
    private String firstName;
    private String lastName;
    private int year;
    private char gender;
    private float mark;

    public Student(String firstName, String lastName, int year, char gender, float mark) {
        setFirstName(firstName);
        setLastName(lastName);
        setYear(year);
        setGender(gender);
        setMark(mark);
    }

    public Student(String[] data) {
        setFirstName(data[0]);
        setLastName(data[1]);
        setYear(Integer.parseInt(data[2]));
        setGender(data[3].charAt(0));
        setMark(Float.parseFloat(data[4]));
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName.length() > 2 && firstName.length() < 20)
            this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName.length() > 2 && lastName.length() < 20)
            this.lastName = lastName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (year > 1980)
            this.year = year;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        if (gender == 'f' || gender == 'm')
            this.gender = gender;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        if (mark > 0 && mark <= 100)
            this.mark = mark;
    }

    public void printInfo() {
        System.out.println("------------------------");
        System.out.println("First name : " + firstName);
        System.out.println("Last name : " + lastName);
        System.out.println("Year : " + year);
        System.out.println("Gender : " + gender);
        System.out.println("Mark : " + mark);
        System.out.println("------------------------");
    }

    public String getFullName() {
        return firstName + ' ' + lastName;
    }

    public boolean isMale() {
        return gender == 'm' ? true : false;
    }
}
