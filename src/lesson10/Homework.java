package lesson10;

import lesson10.Model.Student;
import lesson10.Service.FileService;
import lesson10.Service.StudentService;
import lesson7.Model.Product;
import lesson7.Service.ProductService;

import java.io.IOException;
import java.util.Scanner;

public class Homework {
    public static void main(String[] args) {

        FileService fileService = new FileService();
        StudentService studentService = new StudentService();

        int studentsCount = 0, index = 0;
        String[] read = new String[0];
        try {
            read = fileService.read("C:\\Users\\nazar\\Desktop\\students.txt");
            for (String studentData : read) {
                studentsCount++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Student[] students = new Student[studentsCount];
        for (String studentData : read) {
            String[] data = studentData.split(",");
            students[index++] = new Student(data);
        }

        boolean isMenuActive = true;
        while (isMenuActive) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter command number");
            System.out.println("1. Print full names of students");
            System.out.println("2. Print all male students");
            System.out.println("3. Print all female students who has mark greater then 50.4");
            System.out.println("4. Print student information having the minimal mark");
            System.out.println("5. Print biggest male student information");
            System.out.println("6. Print students sorted by mark");
            System.out.println("7. Print female students sorted by year");
            System.out.println("8. Exit");

            int commandNumber = scanner.nextInt();

            switch (commandNumber) {
                case 1:
                    studentService.printNames(students);
                    break;
                case 2:
                    studentService.printMaleStudents(students);
                    break;
                case 3:
                    studentService.printFemaleMarkGreater50(students);
                    break;
                case 4:
                    studentService.printMinimalMarkStudent(students);
                    break;
                case 5:
                    studentService.printOldestMale(students);
                    break;
                case 6:
                    studentService.printArrayInfo(studentService.sortByMark(students));
                    break;
                case 7:
                     studentService.femaleSortByYear(students);
                    break;
                case 8:
                    isMenuActive = false;
                    System.out.println("Good bye");
                    break;
                default:
                    System.out.println("Invalid command number");
            }

        }

    }


}
