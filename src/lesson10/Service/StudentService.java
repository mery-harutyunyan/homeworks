package lesson10.Service;

import lesson10.Model.Student;
import lesson7.Model.Product;

public class StudentService {

    public void printNames(Student[] students) {
        for (Student student : students) {
            System.out.println(student.getFullName());
        }
    }

    public void printMaleStudents(Student[] students) {
        for (Student student : students) {
            if (student.getGender() == 'm')
                System.out.println(student.getFullName());
        }
    }

    public void printFemaleMarkGreater50(Student[] students) {
        for (Student student : students) {
            if (student.getGender() == 'f' && student.getMark() > 50.4)
                System.out.println(student.getFullName());
        }
    }

    public void printMinimalMarkStudent(Student[] students) {
        Student minMark = students[0];
        for (Student student : students) {
            if (student.getMark() < minMark.getMark())
                minMark = student;
        }

        minMark.printInfo();
    }

    public void printOldestMale(Student[] students) {
        Student oldestMale = null;

        for (int i = 0; i < students.length; i++) {
            if (students[i].isMale()) {
                if (oldestMale == null) {
                    oldestMale = students[i];
                } else if (students[i].getYear() < oldestMale.getYear()) {
                    oldestMale = students[i];
                }
            }
        }

        oldestMale.printInfo();

    }

    public Student[] sortByMark(Student[] students) {
        boolean isActive = true;
        int forCount = 0;

        while (isActive) {
            isActive = false;
            for (int i = 0; i < students.length - 1 - forCount; i++) {
                if (students[i].getMark() > students[i + 1].getMark()) {
                    Student temp = students[i];
                    students[i] = students[i + 1];
                    students[i + 1] = temp;
                    isActive = true;
                }
            }

            forCount++;
        }

        return students;
    }

    public void femaleSortByYear(Student[] students) {
        boolean isActive = true;
        int forCount = 0;

        while (isActive) {
            isActive = false;
            for (int i = 0; i < students.length - 1 - forCount; i++) {
                if (students[i].getYear() > students[i + 1].getYear()) {
                    Student temp = students[i];
                    students[i] = students[i + 1];
                    students[i + 1] = temp;
                    isActive = true;
                }
            }

            forCount++;
        }

        for (Student student : students) {
            if (!student.isMale())
                student.printInfo();
        }
    }

    public void printArrayInfo(Student[] students) {
        System.out.println("------------------------");
        for (int i = 0; i < students.length; i++) {
            System.out.printf("%d. Student name : %s %n", (i + 1), students[i].getFullName());
        }
        System.out.println("------------------------");
    }
}
