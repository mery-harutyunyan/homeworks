package lesson10;

public class Classwork {
    public static void main(String[] args) {
        System.out.println(isNumber("789"));
        System.out.println(isNumber("7fsdf89"));
    }

    public static boolean isNumber(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }
}