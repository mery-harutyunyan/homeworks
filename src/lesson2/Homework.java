package lesson2;

public class Homework {
    public static void main(String[] args) {
        System.out.println("The Arithmetic Operators");
        int a = 19;
        int b = 8;
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("a + b = " + (a + b));
        System.out.println("a - b = " + (a - b));
        System.out.println("a * b = " + (a * b));
        System.out.println("a / b = " + (a / b));
        System.out.println("a % b = " + (a % b));
        a++;
        System.out.println("a++ = " + a);
        a--;
        System.out.println("a-- = " + a);

        System.out.println("________________________");
        System.out.println("The Relational Operators");
        System.out.println("________________________");

        System.out.println("a == b is " + (a == b));
        System.out.println("a != b is " + (a != b));
        System.out.println("a > b is " + (a > b));
        System.out.println("a >= b is " + (a >= b));
        System.out.println("a < b is " + (a < b));
        System.out.println("a <= b is " + (a <= b));

        System.out.println("_____________________");
        System.out.println("The Bitwise Operators");
        System.out.println("_____________________");

        System.out.println("a & b is " + (a & b));
        System.out.println("a | b is " + (a | b));
        System.out.println("a ^ b is " + (a ^ b));
        System.out.println("~a is " + (~a));
        System.out.println("a >> 2 is " + (a >> 2));
        System.out.println("a >>> 2 is " + (a >>> 2));
        System.out.println("a << 2 is " + (a << 2));

        System.out.println("____________________");
        System.out.println("_______Arrays_______");
        System.out.println("____________________");

        int[] simpleArray = new int[5];
        System.out.println("First element of array is " + simpleArray[0]);

        double[] doubleArray = {1.3, 1.4, 1.6};
        System.out.println("Second element of double array is " + doubleArray[1]);

        double[] newDoubleArray;
        newDoubleArray = new double[]{1.3, 1.4, 1.6};
        System.out.println("Third element of double array is " + doubleArray[2]);

    }
}
