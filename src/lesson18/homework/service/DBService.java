package lesson18.homework.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lesson18.homework.model.DBConnection;

import java.io.*;

public class DBService {
    private static final String FILE_NAME = "DBConnection.text";

    public static void serializeDBConnection(DBConnection dbConnection) {
        try (FileOutputStream fos = new FileOutputStream(FILE_NAME);
             ObjectOutput out = new ObjectOutputStream(fos)) {
            out.writeObject(dbConnection);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static DBConnection deserializeDBConnection() {
        DBConnection dbConnection = null;
        try (FileInputStream fis = new FileInputStream(FILE_NAME);
             ObjectInput in = new ObjectInputStream(fis)) {

            dbConnection = (DBConnection) in.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dbConnection;
    }
}
