package lesson18.homework;

import lesson18.homework.model.DBConnection;
import lesson18.homework.service.DBService;

public class Main {
    public static void main(String[] args) {

        DBConnection instance = DBConnection.getDBInstance();
        instance.setHost("localhost");
        instance.setUser("root");
        instance.setDb("picsart");
        instance.setPassword("123456");
        DBService.serializeDBConnection(instance);

        DBConnection deserializedInstance = DBService.deserializeDBConnection();

        System.out.println("Serialized instance hashCode : " + instance.hashCode());
        System.out.println("Deserialized instance hashCode : " + deserializedInstance.hashCode());
    }
}
