package lesson18.homework.model;

import java.io.Serializable;

public class DBConnection implements Serializable {
    private String host;
    private String db;
    private String user = "root";
    private String password = "";
    private static DBConnection instance;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private DBConnection() {
    }

    public static DBConnection getDBInstance() {
        if (instance == null) {
            synchronized (DBConnection.class) {
                if (instance == null) {
                    instance = new DBConnection();
                }
            }
        }
        return instance;
    }

    // The readResolve method allows a class to replace/resolve
    // the object read from the stream before it is returned to the caller
    protected Object readResolve() {
        return instance;
    }
}
