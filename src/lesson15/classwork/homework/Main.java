package lesson15.classwork.homework;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter minutes");
        int minutes = scanner.nextInt();

        System.out.println("Enter seconds");
        int seconds = scanner.nextInt();

        Thread timerThread = new Thread(new TimerRunnable(minutes, seconds));
        timerThread.start();
    }
}
