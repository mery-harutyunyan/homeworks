package lesson15.classwork.homework;

public class TimerRunnable implements Runnable {
    private Integer minutes;
    private Integer seconds;

    public TimerRunnable(Integer minutes, Integer seconds) {
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public Integer getSeconds() {
        return seconds;
    }

    public void setSeconds(Integer seconds) {
        this.seconds = seconds;
    }

    @Override
    public void run() {
        int timeInSeconds = minutes * 60 + seconds;
        int min, sec;
        while (timeInSeconds >= 0) {
            min = (int) timeInSeconds / 60;
            sec = timeInSeconds % 60;
            System.out.println(String.format("%02d", min) + " : " + String.format("%02d", sec));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeInSeconds--;
        }
    }
}
