package examPreparing;

public class Test {
    public static void main(String args[]) {
        System.out.println(factorial(5));
        System.out.println(fibonacci(20));
    }

    public static int factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        }

        return n * factorial(n - 1);
    }

    public static int fibonacci(int n) {

        if (n == 0 || n == 1)
            return n;

        return fibonacci(n - 2) + fibonacci(n - 1);
    }
}