package examPreparing;

public class StaticBlockExample {
    static int age;
    static String name;

    static {
        System.out.println("static block 1");
        age = 2;
        name = "Areg";
    }

    static {
        System.out.println("static block 2");
        age = 30;
        name = "Aram";
    }

    public static void main(String[] args) {
        System.out.println(age);
        System.out.println(name);
    }
}
