package examPreparing;

public class User {
    private String name;
    private int age;

    public User() {
        System.out.println("Constructor");
        name = "Mery";
        age = 30;
    }

    {
        System.out.println("Block 1");
        name = "Areg";
        age = 2;
    }


    public static void main(String[] args) {
        User user = new User();
        System.out.println(user.name);
        System.out.println(user.age);
    }
}
