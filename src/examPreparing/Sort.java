package examPreparing;

import java.util.*;

public class Sort {

    public enum SEASONS {WINTER, SPRING, SUMMER, FALL}



    public static void main(String[] args) {
        ArrayList<Integer> myList = new ArrayList<>();
        myList.add(12);
        myList.add(23);
        myList.add(34);
        myList.add(-4);

        Collections.sort(myList, Collections.reverseOrder());


        TreeSet<String> aa = new TreeSet<>();
        aa.add("ssss");
        aa.add("vvv");
        aa.add("hhh");
        aa.add("ssss");
        aa.add("aa");

        System.out.println(aa);


        HashSet<String> elements = new HashSet<>();
        elements.add("El6");
        elements.add("El4");
        elements.add("El3");
        elements.add("El3");
        elements.add("El4");

        System.out.println(elements);
        Set<String> els = new TreeSet<>(elements);
        System.out.println(els);


        LinkedHashMap<Integer, String> lhm = new LinkedHashMap<>();
        lhm.put(1, "Mery");
        lhm.put(2, "Aram");
        lhm.put(3, "Areg");

        System.out.println(lhm);

        Set<Map.Entry<Integer, String>> set = lhm.entrySet();
        System.out.println(set);
        Iterator<Map.Entry<Integer, String>> iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry<Integer, String> el = (Map.Entry<Integer, String>) iterator.next();
            System.out.println(el.getKey() + "   " + el.getValue());

        }


        for(SEASONS s: SEASONS.values()){
            System.out.println(s);
        }

    }
}
