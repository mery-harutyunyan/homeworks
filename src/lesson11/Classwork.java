package lesson11;

import java.util.ArrayList;
import java.util.Collections;

public class Classwork {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>(Collections.nCopies(10, 4));
        ArrayList<Integer> numbers1 = new ArrayList<>(Collections.nCopies(7, 1));

        numbers.addAll(numbers1);
        System.out.println(numbers);
    }
}
