package lesson14.classwork;

public class MyListIndexOutOfBoundsException extends RuntimeException {
    public MyListIndexOutOfBoundsException() {
        super("Invalid index for MyList");
    }
}
