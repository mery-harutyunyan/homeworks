package lesson14.classwork;

public class MyArrayList<T> implements MyList<T> {
    private static final int CAPACITY = 3;
    private Object[] data;
    private int size;
    private int index;

    public MyArrayList() {
        data = new Object[CAPACITY];
        size = CAPACITY;
    }

    @Override
    public void add(T obj) {
        if (index == size - 1) {
            increaseSize();
        }

        data[index++] = obj;
    }

    private void increaseSize() {
        size = size + CAPACITY;
        Object[] newData = new Object[size];
        System.arraycopy(data, 0, newData, 0, data.length);
        data = newData;
    }

    @Override
    public T get(int index) {
        checkIndex(index);
        return (T) data[index];
    }

    @Override
    public void remove(int index) {
        checkIndex(index);

        for (int i = index; i < this.index; i++) {
            data[i] = data[i + 1];
        }
        data[this.index - 1] = null;
        this.index--;
    }

    @Override
    public void remove(T obj) {
        if (obj == null)
            return;

        for (int i = 0; i < size(); i++) {
            if (obj.equals(data[i]))
                remove(i);
        }

    }

    @Override
    public int size() {
        return index;
    }

    public void checkIndex(int index) {
        if (index < 0 || index >= this.index) {
            throw new MyListIndexOutOfBoundsException();
        }
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < index ; i++) {
            sb.append(data[i].toString());
            if (i != index - 1) {
                sb.append(", ");
            }
        }

        sb.append("]");
        return sb.toString();
    }
}
