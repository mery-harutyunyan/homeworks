package lesson14.classwork;


public class Main {
    public static void main(String[] args) {
        MyArrayList<Integer> a = new MyArrayList<>();
        a.add(2);
        a.add(12);
        a.add(24);
        a.add(22);

        System.out.println(a);
        System.out.println(a.size());
        System.out.println(a.get(2));
//        System.out.println(a.get(222));
        a.remove(1);
        System.out.println(a);
        a.remove(Integer.valueOf(22));
        System.out.println(a);
    }
}
