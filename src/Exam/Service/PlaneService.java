package Exam.Service;


import Exam.Model.Plane;

import java.util.Scanner;

public class PlaneService {
    public Plane createPlane() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter plane model");
        String model = scanner.next();

        System.out.println("Enter plane country");
        String country = scanner.next();

        System.out.println("Enter plane year");
        int year = scanner.nextInt();

        System.out.println("Enter plane hours");
        int hours = scanner.nextInt();

        System.out.println("Enter plane is military (y/n)");
        boolean isMilitary = scanner.next().charAt(0) == 'y';

        System.out.println("Enter plane weight");
        int weight = scanner.nextInt();

        System.out.println("Enter plane top Speed");
        int topSpeed = scanner.nextInt();

        System.out.println("Enter plane seats");
        int seats = scanner.nextInt();

        System.out.println("Enter plane cost");
        float cost = scanner.nextFloat();

        Plane plane = new Plane(model, year, isMilitary, cost);
        plane.setCountry(country);
        plane.setHours(hours);
        plane.setWeight(weight);
        plane.setTopSpeed(topSpeed);
        plane.setSeats(seats);

        return plane;
    }

    public void printDependsIsMilitary(Plane plane) {
        if (plane.isMilitary()) {
            System.out.println(plane.getCost());
            System.out.println(plane.getTopSpeed());
        } else {
            System.out.println(plane.getModel());
            System.out.println(plane.getCountry());
        }
    }

    public Plane newestPlane(Plane plane1, Plane plane2) {
        return plane1.getYear() >= plane2.getYear() ? plane1 : plane2;
    }

    public void printSmallestSeatsCount(Plane plane1, Plane plane2, Plane plane3) {
        Plane withSmallestSeatsCount = plane1.getSeats() < plane2.getSeats() ? plane1 : plane2;

        if (withSmallestSeatsCount.getSeats() > plane3.getSeats())
            withSmallestSeatsCount = plane3;

        System.out.println(withSmallestSeatsCount.getCountry());
    }

    public void printNotMilitaryPlanes(Plane[] planes) {
        for (Plane plane : planes) {
            if (!plane.isMilitary()) {
                plane.printInfo();
            }
        }
    }

    public void printMilitaryWith100Hours(Plane[] planes) {
        for (Plane plane : planes) {
            if (plane.isMilitary() && plane.getHours() > 100) {
                plane.printInfo();
            }
        }
    }

    public Plane minimalWeightPlane(Plane[] planes) {
        Plane minimal = planes[0];

        for (int i = 1; i < planes.length; i++) {
            if (planes[i].getWeight() <= minimal.getWeight())
                minimal = planes[i];
        }

        return minimal;
    }

    public Plane cheapestMilitaryPlane(Plane[] planes) {
        Plane cheapestMilitaryPlane = null;

        for (int i = 0; i < planes.length; i++) {
            if (planes[i].isMilitary()) {
                if (cheapestMilitaryPlane == null) {
                    cheapestMilitaryPlane = planes[i];
                } else if (cheapestMilitaryPlane.getCost() > planes[i].getCost()) {
                    cheapestMilitaryPlane = planes[i];
                }
            }
        }

        return cheapestMilitaryPlane;
    }

    public Plane[] orderByYear(Plane[] planes) {
        boolean isActive = true;
        int forCount = 0;

        while (isActive) {
            isActive = false;
            for (int i = 0; i < planes.length - 1 - forCount; i++) {
                if (planes[i].getYear() > planes[i + 1].getYear()) {
                    Plane temp = planes[i];
                    planes[i] = planes[i + 1];
                    planes[i + 1] = temp;
                    isActive = true;
                }
            }

            forCount++;
        }

        return planes;
    }

    public void printArrayInfo(Plane[] planes) {
        System.out.println("------------------------");
        for (int i = 0; i < planes.length; i++) {
            System.out.printf("%d. Plane model : %s %n", (i + 1), planes[i].getModel());
        }
        System.out.println("------------------------");
    }
}
