package Exam.Model;

public class Plane {

    private String model;
    private String country;
    private int year;
    private int hours;
    private boolean isMilitary;
    private int weight;
    private int topSpeed;
    private int seats;
    private float cost;

    public Plane(String model, int year, boolean isMilitary, float cost) {
        this.setModel(model);
        this.setYear(year);
        this.setMilitary(isMilitary);
        this.setCost(cost);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
//        if(model.equals("")){
//            System.out.println("*************Enter valid model");
//        }
        this.model = model;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
//        if(country.equals("")){
//            System.out.println("*************Enter valid country");
//        }
        this.country = country;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (year > 2020 || year < 1903) {
            System.out.println("*************Enter valid year");
        }
        this.year = year;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if (hours < 0 || hours > 10000) {
            System.out.println("*************Enter valid hours");
        }
        this.hours = hours;
    }

    public boolean isMilitary() {
        return isMilitary;
    }

    public void setMilitary(boolean military) {
        isMilitary = military;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        if (weight < 1000  || weight > 160000 ) {
            System.out.println("*************Enter valid weight");
        }
        this.weight = weight;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        if (topSpeed < 0 || topSpeed > 1000) {
            System.out.println("*************Enter valid top speed");
        }
        this.topSpeed = topSpeed;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        if (hours < 0) {
            System.out.println("*************Enter valid seats");
        }
        this.seats = seats;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(float cost) {
        if (cost < 0) {
            System.out.println("*************Enter valid cost");
        }
        this.cost = cost;
    }

    public void printInfo() {
        System.out.println("------------------------");
        System.out.println("Model : " + model);
        System.out.println("------------------------");
    }
}
