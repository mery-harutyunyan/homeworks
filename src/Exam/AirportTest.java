package Exam;

import Exam.Model.Plane;
import Exam.Service.PlaneService;


import java.util.Scanner;

public class AirportTest {

    public static void main(String[] args) {

        PlaneService service = new PlaneService();
        Plane plane1 = service.createPlane();
        Plane plane2 = service.createPlane();
        Plane plane3 = service.createPlane();
        Plane[] planes = {plane1, plane2, plane3};



        boolean isMenuActive = true;
        while (isMenuActive) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter command number");
            System.out.println("1. Print cost and topSpeed if the plane is military otherwise print model and country");
            System.out.println("2. Return the plane which one is newer (if they have the same age return first one)");
            System.out.println("3. Print country of the plane with smallest seats count (if they have the same - print first)");
            System.out.println("4. Print all not military planes");
            System.out.println("5. Print all military planes which were in air more than 100 hours");
            System.out.println("6. Return the plane with minimal weight (if there are some of them return last one)");
            System.out.println("7. Return the plane with minimal cost from all military planes (if there are some of them return first one)");
            System.out.println("8. Print planes in ascending form order by year");
            System.out.println("9. Exit");

            int commandNumber = scanner.nextInt();

            switch (commandNumber) {
                case 1:
                    service.printDependsIsMilitary(plane1);
                    break;
                case 2:
                    service.newestPlane(plane1, plane2).printInfo();
                    break;
                case 3:
                    service.printSmallestSeatsCount(plane1, plane2, plane3);
                    break;
                case 4:
                    service.printNotMilitaryPlanes(planes);
                    break;
                case 5:
                    service.printMilitaryWith100Hours(planes);
                    break;
                case 6:
                    service.minimalWeightPlane(planes).printInfo();
                    break;
                case 7:
                    service.cheapestMilitaryPlane(planes).printInfo();
                    break;
                case 8:
                    service.printArrayInfo(service.orderByYear(planes));
                    break;
                case 9:
                    isMenuActive = false;
                    System.out.println("Good bye");
                    break;
                default:
                    System.out.println("Invalid command number");
            }

        }

    }
}
