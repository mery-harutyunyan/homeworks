package lesson13.homework;

import lesson13.homework.model.Country;
import lesson13.homework.model.User;
import lesson13.homework.service.UserService;

public class Main {
    public static void main(String[] args) {
        User user = new User("admin", "test", 1990, true, new Country("Armenia"));

        UserService.serializeUser(user);

        User user1 = UserService.deserializeUser();
        System.out.println(user1);
    }
}
