package lesson13.homework.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lesson13.homework.model.User;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;


public class UserService {

    private static final String FILE_NAME = "User.xml";

    public static void serializeUser(User user) {

        XmlMapper xmlMapper = new XmlMapper();

        String xmlString = null;
        try {
            xmlString = xmlMapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        System.out.println(xmlString);

        // write XML string to file
        File xmlOutput = new File(FILE_NAME);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(xmlOutput);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (fileWriter != null)
                fileWriter.write(xmlString);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (fileWriter != null)
                fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static User deserializeUser() {
        User user = null;
        String readContent = null;

        try {
            XmlMapper xmlMapper = new XmlMapper();
            readContent = new String(Files.readAllBytes(Paths.get(FILE_NAME)));

            user = (User) xmlMapper.readValue(readContent, User.class);
         } catch (IOException e) {
            e.printStackTrace();
        }

        return user;
    }
}
