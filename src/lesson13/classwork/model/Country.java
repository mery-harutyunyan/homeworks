package lesson13.classwork.model;

import java.io.Serializable;

public class Country implements Serializable {
    public Country(String name) {
        this.name = name;
    }

    private  String name;

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                '}';
    }
}
