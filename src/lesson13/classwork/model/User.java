package lesson13.classwork.model;

import java.io.Serializable;

public class User  implements Serializable {
    private static final long serialVersionUId = 12121212L;

    private String login;
    private transient String password;
    private int year;
    private boolean isAdmin;

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", year=" + year +
                ", isAdmin=" + isAdmin +
                ", country=" + country +
                '}';
    }

    private Country country;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public User(String login, String password, int year, boolean isAdmin, Country country) {
        this.login = login;
        this.password = password;
        this.year = year;
        this.isAdmin = isAdmin;
        this.country = country;
    }
}
