package lesson13.classwork;

import lesson13.classwork.model.Country;
import lesson13.classwork.model.User;
import lesson13.classwork.service.UserService;

public class main {
    public static void main(String[] args) {

        User user = new User("admin", "test", 1990, true, new Country("Armenia"));

        UserService.serializeUser(user);

        User user1 = UserService.deserializeUser();
        System.out.println(user1);
    }
}
