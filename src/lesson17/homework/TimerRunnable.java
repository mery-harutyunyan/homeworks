package lesson17.homework;

public class TimerRunnable implements Runnable {
    private Integer minutes;
    private Integer seconds;
    private TimeChangeListener timeChangeListener;
    private boolean isStopped = false;

    public TimerRunnable(Integer minutes, Integer seconds, TimeChangeListener timeChangeListener) {
        this.minutes = minutes;
        this.seconds = seconds;
        this.timeChangeListener = timeChangeListener;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public Integer getSeconds() {
        return seconds;
    }

    public void setSeconds(Integer seconds) {
        this.seconds = seconds;
    }

    @Override
    public void run() {
        int timeInSeconds = minutes * 60 + seconds;
        int min, sec;
        while (timeInSeconds >= 0 && !isStopped) {
            min = (int) timeInSeconds / 60;
            sec = timeInSeconds % 60;
            timeChangeListener.onTimeUpdate(min, sec);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeInSeconds--;
        }
    }

    public void stop(){
        isStopped = true;
    }
}
