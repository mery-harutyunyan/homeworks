package lesson17.homework;

public interface TimeChangeListener {
    void onTimeUpdate(int minutes, int seconds);
}
