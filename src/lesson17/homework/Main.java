package lesson17.homework;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class MyFrame extends JFrame implements ActionListener {

    private Container container;
    private JLabel pageTitle;
    private JLabel minutesLabel;
    private JTextField minutesTextField;
    private JLabel secondsLabel;
    private JTextField secondsTextField;

    private JButton submitButton;
    private JButton resetButton;
    private JTextArea minutesTextArea;
    private JTextArea secondsTextArea;
    private TimerRunnable timerRunnable;

    public MyFrame() {
        setTitle("Countdown");
        setBounds(300, 90, 500, 500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        container = getContentPane();
        container.setLayout(null);

        pageTitle = new JLabel("COUNTDOWN");
        pageTitle.setFont(new Font("Arial", Font.PLAIN, 30));
        pageTitle.setSize(240, 30);
        pageTitle.setLocation(130, 30);
        container.add(pageTitle);


        minutesTextField = new JTextField();
        minutesTextField.setHorizontalAlignment(JTextField.CENTER);
        minutesTextField.setFont(new Font("Arial", Font.PLAIN, 20));
        minutesTextField.setSize(50, 50);
        minutesTextField.setLocation(160, 100);
        container.add(minutesTextField);

        minutesLabel = new JLabel("MIN");
        minutesLabel.setFont(new Font("Arial", Font.PLAIN, 20));
        minutesLabel.setForeground(new Color(128, 127, 127));
        minutesLabel.setSize(100, 20);
        minutesLabel.setLocation(165, 160);
        container.add(minutesLabel);

        secondsTextField = new JTextField();
        secondsTextField.setHorizontalAlignment(JTextField.CENTER);
        secondsTextField.setFont(new Font("Arial", Font.PLAIN, 20));
        secondsTextField.setSize(50, 50);
        secondsTextField.setLocation(230, 100);
        container.add(secondsTextField);

        secondsLabel = new JLabel("SEC");
        secondsLabel.setFont(new Font("Arial", Font.PLAIN, 20));
        secondsLabel.setForeground(new Color(128, 127, 127));
        secondsLabel.setSize(100, 20);
        secondsLabel.setLocation(235, 160);
        container.add(secondsLabel);

        submitButton = new JButton("SUBMIT");
        submitButton.setFont(new Font("Arial", Font.PLAIN, 15));
        submitButton.setSize(90, 30);
        submitButton.setBackground(new Color(21, 87, 75));
        submitButton.setForeground(new Color(255, 255, 255));
        submitButton.setBorderPainted(false);
        submitButton.setLocation(125, 200);
        submitButton.addActionListener(this);
        container.add(submitButton);

        resetButton = new JButton("RESET");
        resetButton.setFont(new Font("Arial", Font.PLAIN, 15));
        resetButton.setBackground(new Color(21, 87, 75));
        resetButton.setForeground(new Color(255, 255, 255));
        resetButton.setBorderPainted(false);
        resetButton.setSize(90, 30);
        resetButton.setLocation(235, 200);
        resetButton.addActionListener(this);
        container.add(resetButton);


        minutesTextArea = new JTextArea();
        minutesTextArea.setFont(new Font("Arial", Font.PLAIN, 120));
        minutesTextArea.setSize(150, 150);
        minutesTextArea.setLocation(65, 260);
        minutesTextArea.setLineWrap(true);
        minutesTextArea.setEditable(false);
        container.add(minutesTextArea);


        secondsTextArea = new JTextArea();
        secondsTextArea.setFont(new Font("Arial", Font.PLAIN, 120));
        secondsTextArea.setSize(150, 150);
        secondsTextArea.setLocation(235, 260);
        secondsTextArea.setLineWrap(true);
        container.add(secondsTextArea);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == submitButton) {

            if (timerRunnable != null)
                timerRunnable.stop();

            int minutes = Integer.parseInt(minutesTextField.getText());
            int seconds = Integer.parseInt(secondsTextField.getText());

            TimeChangeListener timeChangeListener = new TimeChangeListener() {
                @Override
                public void onTimeUpdate(int minutes, int seconds) {
                    minutesTextArea.setText(String.format("%02d", minutes));
                    secondsTextArea.setText(String.format("%02d", seconds));
                }
            };
            timerRunnable = new TimerRunnable(minutes, seconds, timeChangeListener);
            Thread timerThread = new Thread(timerRunnable);
            timerThread.start();

        } else if (e.getSource() == resetButton) {
            String def = "00";
            minutesTextField.setText(def);
            secondsTextField.setText(def);

            minutesTextArea.setText(def);
            secondsTextArea.setText(def);

            if (timerRunnable != null)
                timerRunnable.stop();
        }
    }
}

class Main {

    public static void main(String[] args) throws Exception {
        MyFrame f = new MyFrame();


    }
}

