package lesson17.classwork;

import java.util.Date;

public class Human {
    private final String name;
    private final Date birthdate;

    public Human(String name, Date birthdate) {
        this.name = name;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public Date getBirthdate() {
        return birthdate;
    }
}
